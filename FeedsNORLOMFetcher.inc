<?php
// $Id:$

/**
 * @file
 * Home of the FeedsFileFetcher and related classes.
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsFileFetcher.
 */
class FeedsNORLOMBatch extends FeedsImportBatch {
  protected $file_path;
  protected $xml_filename = 'imsmanifest.xml';
  protected $zip_error = array(
    ZIP_ER_OK =>'N No error',
    ZIP_ER_MULTIDISK =>'N Multi-disk zip archives not supported',
    ZIP_ER_RENAME =>'S Renaming temporary file failed',
    ZIP_ER_CLOSE =>'S Closing zip archive failed',
    ZIP_ER_SEEK =>'S Seek error',
    ZIP_ER_READ =>'S Read error',
    ZIP_ER_WRITE =>'S Write error',
    ZIP_ER_CRC =>'N CRC error',
    ZIP_ER_ZIPCLOSED =>'N Containing zip archive was closed',
    ZIP_ER_NOENT =>'N No such file',
    ZIP_ER_EXISTS =>'N File already exists',
    ZIP_ER_OPEN =>'S Can\'t open file',
    ZIP_ER_TMPOPEN =>'S Failure to create temporary file',
    ZIP_ER_ZLIB =>'Z Zlib error',
    ZIP_ER_MEMORY =>'N Malloc failure',
    ZIP_ER_CHANGED =>'N Entry has been changed',
    ZIP_ER_COMPNOTSUPP =>'N Compression method not supported',
    ZIP_ER_EOF =>'Premature EOF',
    ZIP_ER_INVAL =>'N Invalid argument',
    ZIP_ER_NOZIP =>'N Not a zip archive',
    ZIP_ER_INTERNAL =>'N Internal error',
    ZIP_ER_INCONS =>'N Zip archive inconsistent',
    ZIP_ER_REMOVE =>'S Can\'t remove file',
    ZIP_ER_DELETED =>'N Entry has been deleted',
  );

  /**
   * Constructor.
   */
  public function __construct($file_path) {
    $this->file_path = $file_path;
    $this->xml_filename = 'imsmanifest.xml';
    parent::__construct();
  }

  /**
   * Implementation of FeedsImportBatch::getRaw();
   */
  public function getRaw() {
    // Get the uploaded file and find the imsmanifest.xml file anf the thumbs from there
    $path_parts = pathinfo($this->file_path);
    $xml = NULL;
//    $bt_xml = @ new SimpleXMLElement($xml);
//    $st_xml = @ new SimpleXMLElement($xml);

    if (function_exists('zip_open') && $this->file_path != '') {

      $zip = zip_open(realpath($this->file_path));

      $zip_path = '';
      if (is_resource($zip)) {
        while ($zip_entry = zip_read($zip)) {
          $zipdata[] = array(
            "Name:               " => zip_entry_name($zip_entry),
            "Actual Filesize:    " => zip_entry_filesize($zip_entry),
            "Compressed Size:    " => zip_entry_compressedsize($zip_entry),
            "Compression Method: " => zip_entry_compressionmethod($zip_entry)
          );

          if (zip_entry_open($zip, $zip_entry, "r")) {
            switch (zip_entry_name($zip_entry)) {
              case $zip_path . $this->xml_filename:
                $xml = @ new SimpleXMLElement(zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
                break;

              case $zip_path . 'big_thumb.gif':
              case $zip_path . 'big_thumb.png':
                $big_thumb_url = $this->extract_and_save_file(zip_entry_name($zip_entry), zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
                // $container = new FeedsEnclosure($file_url, 'mime/gif');
                break;

              case $zip_path . 'small_thumb.gif':
              case $zip_path . 'small_thumb.png':
                $small_thumb_url = $this->extract_and_save_file(zip_entry_name($zip_entry), zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
                break;

              case 'ims/':
                // This is a subdirectory of the original zip file
                $zip_path .= 'ims/';

              default:
                break;
            }
            zip_entry_close($zip_entry);


          }
          else {
            drupal_set_message(t("Error in opening zip file entry: @filename", array("@filename" => zip_entry_name($zip_entry))), 'error');
          }
        }

        zip_close($zip);

        if ($xml) {
          if (is_object($xml->metadata)) {
            $xml->addChild('metadata');
          }
          $xml->metadata->addChild('big_thumb', $big_thumb_url); // Add this to the raw feed of xml data to be parsed by Norlomparser
          $xml->metadata->addChild('small_thumb', $small_thumb_url); // Add this to the raw feed of xml data to be parsed by Norlomparser
          $xml->metadata->addChild('dlr_file', $this->file_path); // Add this to the raw feed of xml data to be parsed by Norlomparser
        }
        else {
          drupal_set_message(t('No XML file loaded. Is the DLR correctly composed? Verify @filename that all parts are according to standard.', array('@filename' => $this->file_path)), 'error');
          return null;
        }
      }
      else
        drupal_set_message(t("Error in zip file: @error_msg", array('@error_msg' => $zip_error[$zip])), 'error');
    }

    if (!function_exists('zip_open')) {
      drupal_set_message('PHP Zip library not installed. NORLOM Loader requires PHP Zip library installed. Read more at: <a href="http://no.php.net/manual/en/zip.installation.php">Zip Installation</a>', 'error');
    }
//  drupal_set_message("<pre>".print_r($buf, true)."</pre>");
//    drupal_set_message("<pre>".print_r($zipdata, true)."</pre>");

      return $xml->asXML();
  }

  /**
   * Implementation of FeedsImportBatch::getFilePath().
   */
  public function getFilePath() {
    return $this->file_path;
  }

  public function extract_and_save_file($filename, $binarystream) {

    $path_parts = pathinfo($this->file_path);

    if(!isset($this->file)) {
      // Get path of files directory
      $filepath = file_destination($path_parts['dirname'] .'/'. $path_parts['filename'] .'-'. basename($filename), FILE_EXISTS_RENAME);
      $file = file_save_data($binarystream, $filepath);
      if ($file === 0) {
        throw new Exception(t('Cannot write content to %dest', array('%dest' => $filepath)));
      }
    }

    return $filepath;
  }
}

/**
 * Fetches data via HTTP.
 */
class FeedsNORLOMFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    return new FeedsNORLOMBatch($source_config['source']);
  }

  /**
   * Implementation of the FeedsFetcher::request() method
   *
   * @param unknown_type $feed_nid
   *   If a node is being populated, this is the node id
   *
   * @return
   *   A string to be returned to the client.
   */
  public function request($feed_nid = 0) {
    $feed_dir = file_directory_path() .'/feeds';
    file_check_directory($feed_dir, TRUE);

    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', null, 200);

    foreach ($_FILES as $fieldName => $file) {

      $uploadedname =   $feed_dir. "/" . strip_tags(basename($file['name']));

      if ($path = file_check_location($uploadedname, $feed_dir)) {
  //      echo '{"status":"error", "message":"file_check_location: File already found."}';
  //      exit();
      }

      move_uploaded_file($file['tmp_name'], $uploadedname);

      // Create the object
      $feedObject = feeds_source($importer_id, 0);
      // Set the file name to import
      $feedObject->setConfig(array('FeedsNORLOMFetcher' => array('source' => $uploadedname)));
      $feedObject->save();

      $i = 0;

      do {
        // Run the import process
        $status = $feedObject->existing()->import();
        $i++;
      } while  ($status != FEEDS_BATCH_COMPLETE && $i < 1000);

    }

    // Get the status messages created by the import
    $i = count($_SESSION['messages']['status']);
    $msg = "Ok"; // Defualt message when uploading

    // If there are messages
    if ($i) {
      // Get the latest message
      $msg = $_SESSION['messages']['status'][$i-1];
    }

    echo '{"status":"ok", "message":"'.check_plain($msg).'"}';
    // echo '{"status":"error", "message":"Error message why this failed"}';

  }

  public function configForm(&$form_state) {
    $form = array();
    $form['xmlfile'] = array(
      '#type' => 'textfield',
      '#title' => t('XML Source'),
      '#default_value' => $this->xml_filename,
      '#description' => t('File name containing the DLR data to extract.'),
    );
    return $form;
  }

  public function configFormValidate(&$values) {
//    $this->xml_filename = $values['value'];
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {

    $form = array();

    // When renaming, do not forget feeds_vews_handler_field_source class.
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('File'),
      '#description' => t('Specify a file in the site\'s file system path or upload a file below.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
    );

    $form['upload'] = array(
      '#type' => 'file',
      '#title' => t('Upload'),
      '#description' => t('Choose a file from your local computer.'),
    );

    //Get the importer-id
    $params = explode("/", $_GET['q']);

    $form['info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Multiple file upload'),
      '#description' =>         t('To upload  multiple files in one go, please use the multiple file uploader at
        the <a href="@location">Multiple file upload</a> page',
        array('@location' =>drupal_get_normal_path(base_path(). '?q=' . 'import/'.$params[1].'/multiload'))),

    );

    return $form;
}

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $feed_dir = file_directory_path() .'/feeds';
    file_check_directory($feed_dir, TRUE);

    // If there is a file uploaded, save it, otherwise validate input on
    // file.
    if ($file = file_save_upload('feeds', array(), $feed_dir)) {
      file_set_status($file, FILE_STATUS_PERMANENT);
      $values['source'] = $file->filepath;
    }
    elseif (empty($values['source'])) {
      form_set_error('feeds][source', t('Upload a file first.'));
    }
    // If a file has not been uploaded and $values['source'] is not empty, make
    // sure that this file is within Drupal's files directory as otherwise
    // potentially any file that the web server has access could be exposed.
    elseif (!file_check_location($values['source'], file_directory_path())) {
      form_set_error('feeds][source', t('File needs to point to a file in your Drupal file system path.'));
    }

  }
}


