// Variable for holding the filelist.
var fileList;
var uploader;

YAHOO.util.Event.throwErrors = true;

YAHOO.util.Event.onDOMReady(function() {
    var uiLayer = YAHOO.util.Dom.getRegion('selectLink');
    var overlay = YAHOO.util.Dom.get('uploaderOverlay');
    var host = location.host + (location.port ? (":" + location.port) : "");
    var fileIdHash, dataArr;

    YAHOO.util.Dom.setStyle(overlay, 'width', uiLayer.right - uiLayer.left + "px");
    YAHOO.util.Dom.setStyle(overlay, 'height', uiLayer.bottom - uiLayer.top + "px");

    // Custom URL for the uploader swf file (same folder).

    YAHOO.widget.Uploader.SWFURL = "http://" + host + "/sites/all/modules/feeds_norlomloader/yui/uploader.swf";

    // Instantiate the uploader and write it to its placeholder div.
    uploader = new YAHOO.widget.Uploader("uploaderOverlay");

    // Add event listeners to various events on the uploader.
    // Methods on the uploader should only be called once the
    // contentReady event has fired.

    uploader.addListener('contentReady', handleContentReady);
    uploader.addListener('fileSelect', onFileSelect)
    uploader.addListener('uploadStart', onUploadStart);
    uploader.addListener('uploadProgress', onUploadProgress);
    uploader.addListener('uploadCancel', onUploadCancel);
    uploader.addListener('uploadComplete', onUploadComplete);
    uploader.addListener('uploadCompleteData', onUploadResponse);
    uploader.addListener('uploadError', onUploadError);
    uploader.addListener('rollOver', handleRollOver);
    uploader.addListener('rollOut', handleRollOut);

    // When the mouse rolls over the uploader, this function
    // is called in response to the rollOver event.
    // It changes the appearance of the UI element below the Flash overlay.
    function handleRollOver() {
        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'color', "#FFFFFF");
        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'background-color', "#000000");
    }

    // On rollOut event, this function is called, which changes the
    // appearance of the
    // UI element below the Flash layer back to its original state.
    function handleRollOut() {
        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'color', "#0000CC");
        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'background-color', "#FFFFFF");
    }

    // When contentReady event is fired, you can call methods on the
    // uploader.
    function handleContentReady() {
        // Allows the uploader to send log messages to trace, as well as to
        // YAHOO.log
        uploader.setAllowLogging(true);

        // Allows multiple file selection in "Browse" dialog.
        uploader.setAllowMultipleFiles(true);

        // New set of file filters.
        var ff = new Array(
            {
                description : "DLRs",
                extensions : "*.zip"
            }
        );

        // Apply new set of file filters to the uploader.
        uploader.setFileFilters(ff);
    }

    // Fired when the user selects files in the "Browse" dialog
    // and clicks "Ok".
    function onFileSelect(event) {
        if ('fileList' in event && event.fileList != null) {
            fileList = event.fileList;
            createDataTable(fileList);
        }
    }

    function createDataTable(entries) {
        rowCounter = 0;
        fileIdHash = {};
        dataArr = [];
        for (var i in entries) {
            var entry = entries[i];
            entry["progress"] = "<div style='height:5px;width:100px;background-color:#CCC;'></div>";
            dataArr.unshift(entry);
        }

        for (var j = 0; j < dataArr.length; j++) {
            fileIdHash[dataArr[j].id] = j;
        }

        var myColumnDefs = [
            {
                key: "name",
                label: "File Name",
                sortable: false
            },
            {
                key: "size",
                label: "Size",
                sortable: false
            },
            {
                key: "progress",
                label: "Upload progress",
                sortable: false
            },
            {
                key: "status",
                label: "Processing staus"
            }
        ];

        this.myDataSource = new YAHOO.util.DataSource(dataArr);
        this.myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
        this.myDataSource.responseSchema = {
            fields : [ "id", "name", "created", "modified", "type", "size",
                    "progress", "status" ]
        };

        this.singleSelectDataTable = new YAHOO.widget.DataTable(
                "dataTableContainer", myColumnDefs, this.myDataSource, {
                    caption : "Files To Upload",
                    selectionMode : "single"
                });
    }

    // Do something on each file's upload start.
    function onUploadStart(event) {

    }

    // Do something on each file's upload progress event.
    function onUploadProgress(event) {
        rowNum = fileIdHash[event["id"]];
        prog = Math.round(100 * (event["bytesLoaded"] / event["bytesTotal"]));
        progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:"
                + prog + "px;'></div></div>";
        singleSelectDataTable.updateRow(rowNum, {
            name: dataArr[rowNum]["name"],
            size: dataArr[rowNum]["size"],
            progress: progbar,
            status: "Processing..."
        });
    }

    // Do something when each file's upload is complete.
    function onUploadComplete(event) {
        // alert(event);
        rowNum = fileIdHash[event["id"]];
        prog = Math.round(100 * (event["bytesLoaded"] / event["bytesTotal"]));
        progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:100px;'></div></div>";
        singleSelectDataTable.updateRow(rowNum, {
            name: dataArr[rowNum]["name"],
            size: dataArr[rowNum]["size"],
            progress: progbar,
            status: "Processing..."
        });

    }

    // Do something if a file upload throws an error.
    // (When uploadAll() is used, the Uploader will
    // attempt to continue uploading.
    function onUploadError(event) {

        rowNum = fileIdHash[event["id"]];
        prog = Math.round(100 * (event["bytesLoaded"] / event["bytesTotal"]));
        progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:100px;'></div></div>";
        msgStatus = "<div class='message error'>" + event.status + "</div>";
        singleSelectDataTable.updateRow(rowNum, {
            name: dataArr[rowNum]["name"],
            size: dataArr[rowNum]["size"],
            progress: progbar,
            status: msgStatus
        });
    }

    // Do something if an upload is cancelled.
    function onUploadCancel(event) {

    }

    // Do something when data is received back from the server.
    function onUploadResponse(event) {

        var jsonString = event.data,
            rowNum, prog, progbar, msgStatus, msgError;

        // Parsing JSON strings can throw a SyntaxError exception, so we wrap the call
        // in a try catch block
        try {
            var prod = YAHOO.lang.JSON.parse(jsonString);

            // We can now interact with the data
            if (prod.status == 'ok') {
                rowNum = fileIdHash[event["id"]];
                prog = Math.round(100 * (event["bytesLoaded"] / event["bytesTotal"]));
                progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:100px;'></div></div>";
                msgStatus = "<div class='message status'>" + prod.message + "</div>";
                singleSelectDataTable.updateRow(rowNum, {
                    name: dataArr[rowNum]["name"],
                    size: dataArr[rowNum]["size"],
                    progress: progbar,
                    status: msgStatus
                });
            }

            if (prod.status == 'error') {
                rowNum = fileIdHash[event["id"]];
                prog = Math.round(100 * (event["bytesLoaded"] / event["bytesTotal"]));
                progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:100px;'></div></div>";
                msgError = "<div class='message error'>" + prod.message + "</div>";
                singleSelectDataTable.updateRow(rowNum, {
                    name: dataArr[rowNum]["name"],
                    size: dataArr[rowNum]["size"],
                    progress: progbar,
                    status: msgError
                });
            }

        }
        catch (e) {

            if (window.console) {
                console.debug(e);
                console.debug(event);
                console.debug(event.data);
            }

            rowNum = fileIdHash[event["id"]];
            singleSelectDataTable.updateCell(singleSelectDataTable.getRecord(rowNum), "status", "Server error");
        }
    }
});
